package ru.t1.artamonov.tm.command.task;

import ru.t1.artamonov.tm.api.service.IProjectTaskService;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.command.AbstractCommand;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
