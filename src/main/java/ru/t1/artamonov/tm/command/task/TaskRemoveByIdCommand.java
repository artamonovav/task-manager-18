package ru.t1.artamonov.tm.command.task;

import ru.t1.artamonov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-remove-by-id";

    private static final String DESCRIPTION = "Remove task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(id);
    }

}
