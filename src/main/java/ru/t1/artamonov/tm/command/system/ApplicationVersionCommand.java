package ru.t1.artamonov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private static final String NAME = "version";

    private static final String ARGUMENT = "-v";

    private static final String DESCRIPTION = "Display program version.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

}
