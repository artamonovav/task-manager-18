package ru.t1.artamonov.tm.command.user;

import ru.t1.artamonov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private static final String NAME = "update-user-profile";

    private static final String DESCRIPTION = "update profile of current user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.print("LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.print("MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

}
