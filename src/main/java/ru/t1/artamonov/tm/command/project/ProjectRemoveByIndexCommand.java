package ru.t1.artamonov.tm.command.project;

import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-remove-by-index";

    private static final String DESCRIPTION = "Remove project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }

}
