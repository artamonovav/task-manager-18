package ru.t1.artamonov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "logout";

    private static final String DESCRIPTION = "logout current user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
