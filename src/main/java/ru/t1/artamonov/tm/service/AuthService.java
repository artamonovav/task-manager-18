package ru.t1.artamonov.tm.service;

import ru.t1.artamonov.tm.api.service.IAuthService;
import ru.t1.artamonov.tm.api.service.IUserService;
import ru.t1.artamonov.tm.exception.field.LoginEmptyException;
import ru.t1.artamonov.tm.exception.field.PasswordEmptyException;
import ru.t1.artamonov.tm.exception.user.AccessDeniedException;
import ru.t1.artamonov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}
